# Tensorflow_RecSys

using tensorflow to implement the popular recommender system algorithm.

## Factorization Machine
### Reference.
1. [Factorization Machines](https://www.csie.ntu.edu.tw/~b97053/paper/Rendle2010FM.pdf)

### class tfrs.fm.factorization_machine

Implementation of Regression or Classification with Factorization Machine.

#### __init__ 
```
Params.
1. feat_size: int. 
    numbers of features.
    e.g. total user id + total item id
2. field_size: int. 
    numbers of field for input placeholder.
    e.g. if 2: [user_id, max_user_id + item_id]
3. emb_size: int.
    size of embedding vectors.
4. l2_reg: float.
    hyperparameter of l2 reguralizer.
5. loss_type: str.
    ["log_loss"|"mse"]
    if log_loss, it will be classifier, else if mse, it will be regressor.
6. optimizer: str.
    ["adam"|"adagrad"|"gd"|"mometum"]
    type of common optimizer.
7. learning_rate: float.
    step size for each training optimization.
8. epochs: int.
    number of epochs for training optimization.
9. batch_size: int.
    size of batch.
10. verbose: bool.
    if True, train_writer and testing_writer will be created to save the model during training
11. random_seed: int.
    random seed for tensorflow.
12. train_writer: str.
    folder to save the training summary.
13. test_writer: str.
    folder to save the testing summary
14. log_device_placement: bool.
    show which device (CPU/GPU/...) tensorflow is using for debugging.
```
#### fit
```
Params.
1. training_index: np.ndarray.
    with shape=([None, field_size]).
    e.g. user_id=27, item_id=340 (max user_id = 100) => ([27, 439]).
2. training_value: np.ndarray.
    with shape=([None, field_size]).
    values with respective to the above index.
3. training_label: np.ndarray.
    with shape=([None, 1]).
    dependent variables of the above inputs.
4. valid_index: np.ndarray.
    with shape=([None, field_size]).
5. valid_value: np.ndarray.
    with shape=([None, field_size]).
    values with respective to the above index.
6. valid_label: np.ndarray.
    with shape=([None, 1]).
    dependent variables of the above inputs.
7. early_stopping: int.
    number of times if the val_loss is not changed to trigger the early stopping
8. save_step: int.
    number of steps to save the model.
9. save_path: int.
    folder to save the tmp models.
10. summary_step: int.
    number of batch steps to save the summary of model.

Return.
1. history: dict.
    a dictionary save the loss [and val_loss] per epoch during training.
```
#### predict
```
1. predict_index: np.ndarray
    with shape=([None, field_size]).
2. predict_value: np.ndarray
    with shape=([None, field_size]).

Return.
1. predict: np.ndarray
    with shape=([None, 1]).
    prediction of the above inputs by Factorization Machine.
```


## Matrix Factorization
### Reference.
1. [BPR: Bayesian Personalized Ranking from Implicit Feedback](https://arxiv.org/pdf/1205.2618.pdf)

### class tfrs.mf.mf_bpr
Implementation of Bayesian Personalized Ranking with Deep FM.
#### __init__
```
to be updated
```
#### fit
```
to be updated
```
#### predict
```
to be updated
```
#### similar
```
to be updated
```
#### recommend
```
to be updated
```


## Deep FM
### Reference.
1. [DeepFM: A Factorization-Machine based Neural Network for CTR Prediction](https://www.ijcai.org/proceedings/2017/0239.pdf)


## Field-Aware
### Reference.
1. [Field-aware Factorization Machines for CTR Prediction](https://www.csie.ntu.edu.tw/~cjlin/papers/ffm.pdf)


## Neural Factorization Machine
### Reference.
1. [Neural Factorization Machines for Sparse Predictive Analytics](https://arxiv.org/pdf/1708.05027.pdf)


# TODO.
1. write a negative sampler for MF-BPR
2. write the documentation of MF-BPR
3. implement function FM-BPR
3. implement DeepFM
4. implement FFM
5. implement NFM
6. generate some result to compare different algorithm with sample dataset, like movielen.