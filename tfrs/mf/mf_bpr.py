import numpy as np
import os
from sklearn.base import BaseEstimator, TransformerMixin
import tensorflow as tf
from tensorflow.contrib.tensorboard.plugins import projector
import time
try:
    get_ipython
    from tqdm import tqdm_notebook as tqdm
except:
    from tqdm import tqdm

'''
add tf.summary
create fit_on_batch, fit, predict (by input, by user_id, by item_id)
https://www.cnblogs.com/pinard/p/9163481.html
'''

class MatrixFactorizationBPR(BaseEstimator, TransformerMixin):
    def __init__(self, user_size, item_size,
                 emb_size=8,
                 l2_reg=0.01,
                 optimizer="gd",
                 learning_rate=0.01,
                 epochs=10,
                 batch_size=256,
                 verbose=False,
                 random_seed=2012,
                 train_writer="./model",
                 test_writer=None,
                 log_device_placement=False):
        self.user_size = user_size
        self.item_size = item_size
        self.emb_size = emb_size
        
        self.l2_reg = l2_reg
        self.optimizer_type = optimizer
        self.learning_rate=learning_rate
        
        self.epochs = epochs
        self.batch_size = batch_size

        self.verbose = verbose
        self.random_seed = random_seed
        
        self.train_writer_path = train_writer
        self.test_writer_path = test_writer
        self.log_device_placement = log_device_placement
        
        self._init_graph()

    
    def _init_graph(self):
        # reset the default graph to prevent the error between two graphs.
        tf.reset_default_graph()

        # create a new graph for the class and set it as default graph.
        self.graph = tf.Graph()

        with self.graph.as_default():
            # fixed the random_seed
            tf.set_random_seed(self.random_seed)

            # create placeholder
            self.user_input = tf.placeholder(tf.int32, [None, 1], name="user_input")
            self.pos_item_input = tf.placeholder(tf.int32, [None, 1], name="positive_item_input")
            self.neg_item_input = tf.placeholder(tf.int32, [None, 1], name="negative_item_input")

            # initilize weights of model
            self.weights = self._init_weights()

            # get embedding vector: shape=(None, 1, embedding_size)
            with tf.name_scope("Embedding"):
                u_emb = tf.nn.embedding_lookup(self.weights["user_emb"], 
                                               self.user_input, 
                                               name="user_embedding")
                i_emb = tf.nn.embedding_lookup(self.weights["item_emb"], 
                                               self.pos_item_input,
                                               name="positive_item_embedding")
                j_emb = tf.nn.embedding_lookup(self.weights["item_emb"], 
                                               self.neg_item_input,
                                               name="negative_item_embedding")

            # compute the result of yui > yuj: shape=(None, 1)
            with tf.name_scope("Prediction"):
                self.pred = tf.reduce_sum(tf.multiply(u_emb, (i_emb - j_emb)), axis=2, name="prediction")

            # compute auc of the prediction
            with tf.name_scope("Mertics"):
                self.auc = tf.reduce_mean(tf.to_float(self.pred > 0), name="area_under_curve")

            # regularizer
            with tf.name_scope("Regularization"):
                reg = 0
                reg += tf.nn.l2_loss(u_emb)
                reg += tf.nn.l2_loss(i_emb)
                reg += tf.nn.l2_loss(j_emb)
                reg *= self.l2_reg
                reg = tf.identity(reg, name="regularizer")

            # compute the loss
            with tf.name_scope("Loss"):
                self.loss = reg - tf.reduce_mean(tf.log_sigmoid(self.pred))
                self.loss = tf.identity(self.loss, name="loss")

            # create the optimizer of the graph
            with tf.name_scope("Train_Op"):
                if self.optimizer_type == "gd":
                    self.optimizer = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(self.loss)

            self.saver = tf.train.Saver()
            self.sess = tf.Session(config=tf.ConfigProto(log_device_placement=self.log_device_placement))
            self.sess.run(tf.global_variables_initializer())
            
            # verbose:
            if bool(self.verbose):
                # number of params
                self.total_parameters = 0
                for v in self.weights.values():
                    shape = v.get_shape()
                    vparams = 1
                    for dim in shape:
                        vparams *= dim.value
                    self.total_parameters += vparams
                print(f"total_parameters of the model: {self.total_parameters}")
                
                # tensorboard: tf.summary
                if self.train_writer_path is not None:
                    # summary of outputs
                    tf.summary.scalar("auc", self.auc, collections=[tf.GraphKeys.SUMMARIES])
                    tf.summary.scalar("loss", self.loss, collections=[tf.GraphKeys.SUMMARIES])
                    self.summary = tf.summary.merge(tf.get_collection(tf.GraphKeys.SUMMARIES))
                    self.train_writer = tf.summary.FileWriter(self.train_writer_path, self.sess.graph)
                    if self.test_writer_path is not None:
                        self.test_writer = tf.summary.FileWriter(self.test_writer_path)
    
    
    def _init_weights(self):
        weights = dict()
        with tf.name_scope("Weights"):
            weights["user_emb"] = tf.get_variable("user_embedding",
                                                  [self.user_size, self.emb_size],
                                                  initializer=tf.random_normal_initializer(0, 0.1))
            weights["item_emb"] = tf.get_variable("item_embedding",
                                                  [self.item_size, self.emb_size],
                                                  initializer=tf.random_normal_initializer(0, 0.1))
        return weights
    
    
    def fit_on_batch(self, user_input, pos_item_input, neg_item_input):
        feed_dict = {self.user_input: user_input,
                     self.pos_item_input: pos_item_input,
                     self.neg_item_input: neg_item_input}
        loss, auc, _ = self.sess.run([self.loss, self.auc, self.optimizer], feed_dict=feed_dict)
        return loss, auc
    
    
    def fit_on_batch_with_summary(self, user_input, pos_item_input, neg_item_input):
        feed_dict = {self.user_input: user_input,
                     self.pos_item_input: pos_item_input,
                     self.neg_item_input: neg_item_input}
        summary, loss, auc, _ = self.sess.run([self.summary, self.loss, self.auc, self.optimizer], feed_dict=feed_dict)
        return summary, loss, auc
    
    
    def fit(self, user_input, pos_item_input, neg_item_input, 
            valid_user_input=None, 
            valid_pos_item_input=None, 
            valid_neg_item_input=None,
            early_stopping=False,
            save_step=0,
            save_path=None,
            summary_step=False,
            summary_embedding=False):
        
        has_valid = valid_user_input is not None
        
        history = dict()
        history["auc"] = []
        history["loss"] = []
        if has_valid:
            history["val_auc"] = []
            history["val_loss"] = []
        
        if has_valid and bool(early_stopping) is True:
            best_loss = np.inf
            unchange_time = 0
        
        if bool(self.verbose) and save_path is not None:
            if not os.path.exists(save_path+"/tmp_mdls"):
                os.makedirs(save_path+"/tmp_mdls")
            if not os.path.exists(save_path+"/best_mdls"):
                os.makedirs(save_path+"/best_mdls")
            if not os.path.exists(save_path+"/last_mdls"):
                os.makedirs(save_path+"/last_mdls")
        
        count_summary_step = 0
        samples_size = user_input.shape[0]
        
        for epoch in range(self.epochs):
            print(f"Epoch {epoch+1}/{self.epochs}")
            t1 = time.time()
            auc_, loss_ = [], []
            perm = np.random.permutation(samples_size)
            total_batch = samples_size // self.batch_size + 1
            pbar = tqdm(range(total_batch), desc="avg loss of batch: ?.????")
            for i in pbar:
                start_idx = self.batch_size * i
                end_idx = self.batch_size * (i + 1)
                if end_idx > samples_size:
                    sample_idx = perm[start_idx:]
                else:
                    sample_idx = perm[start_idx:end_idx]
                if bool(self.verbose):
                    summary, loss, auc = self.fit_on_batch_with_summary(user_input,
                                                                        pos_item_input,
                                                                        neg_item_input)
                else:
                    loss, auc = self.fit_on_batch(user_input,
                                                  pos_item_input,
                                                  neg_item_input)
                pbar.set_description(f"avg loss of batch: {loss:.4f}")
                pbar.refresh()

                total_auc = sample_idx.shape[0] * auc
                total_loss = sample_idx.shape[0] * loss
                auc_.append(total_auc)
                loss_.append(total_loss)

                if bool(self.verbose) and bool(summary_step) and (i+1) % int(summary_step) == 0:
                    self.train_writer.add_summary(summary, count_summary_step)
                    if has_valid and self.test_writer_path is not None:
                        val_summary, _, _ = self._evaluate(valid_user_input,
                                                           valid_pos_item_input,
                                                           valid_neg_item_input)
                        self.test_writer.add_summary(val_summary, count_summary_step)
                    count_summary_step += 1
        
            # compute
            avg_auc = np.sum(auc_) / samples_size
            avg_loss = np.sum(loss_) / samples_size
            history["auc"].append(avg_auc)
            history["loss"].append(avg_loss)
            
            t2 = time.time() - t1

            if bool(self.verbose) and self.train_writer_path is not None:
                self.train_writer.add_summary(summary, count_summary_step)
            
            if bool(self.verbose) and bool(save_step) and save_path is not None:
                if (epoch + 1) % int(save_step) == 0:
                    self.saver.save(self.sess, save_path+"/tmp_mdls/mkdel.ckpt", global_step=epoch)
        
            if bool(self.verbose) and has_valid:
                val_summary, val_loss, val_auc = self._evaluate(valid_user_input,
                                                                valid_pos_item_input,
                                                                valid_neg_item_input)
                history["val_auc"].append(val_auc)
                history["val_loss"].append(val_loss)
                if self.test_writer_path is not None:
                    self.test_writer.add_summary(val_summary, count_summary_step)
            
                print(f"{t2:.2f}s - avg auc: {avg_auc:.4f} - avg loss: {avg_loss:.4f} - val auc: {val_auc:.4f} - val loss: {val_loss:.4f}")
            
                if int(early_stopping) > 0:
                    if val_loss < best_loss:
                        best_loss = val_loss
                        unchange_time = 0
                        if save_path is not None:
                            self.saver.save(self.sess, save_path+"/best_mdls/model.ckpt")
                    else:
                        unchange_time += 1
                        if unchange_time >= int(early_stopping):
                            print(f"Early Stop is triggered with unchange time = {unchange_time}")
            else:
                print(f"{t2:.2f}s - avg auc: {avg_auc:.4f} - avg loss: {avg_loss:.4f}")
        
            count_summary_step+=1
        
        if bool(self.verbose) and save_path is not None:
            self.saver.save(self.sess, save_path+"/last_mdls/model.ckpt")
        
        # save the embedding for visualization
        if bool(self.verbose) and bool(summary_embedding):
            '''need to create model.tsv file!'''
            embedding_path = save_path+"/embedding"
            embedding_writer = tf.summary.FileWriter(embedding_path)
            projector_config = projector.ProjectorConfig()
            user_embedding_summary = projector_config.embeddings.add()
            user_embedding_summary.tensor_name = self.weights["user_emb"].name
            user_embedding_summary.metadata_path = "user_metadata.tsv"
            item_embedding_summary = projector_config.embeddings.add()
            item_embedding_summary.tensor_name = self.weights["item_emb"].name
            item_embedding_summary.metadata_path = "item_metadata.tsv"
            projector.visualize_embeddings(embedding_writer, projector_config)
            self.saver.save(self.sess, embedding_path+"/model.ckpt", global_step=0)
        
        # save the trained weights of matrix factorization to np.ndarray
        self.user_emb = self.sess.run(self.weights["user_emb"])
        self.item_emb = self.sess.run(self.weights["item_emb"])
                
        return history

    def _evaluate(self, valid_user_input, valid_pos_item_input, valid_neg_item_input):
        feed_dict = {self.user_input: valid_user_input,
                     self.pos_item_input: valid_pos_item_input,
                     self.neg_item_input: valid_neg_item_input}
        summary, loss, auc = self.sess.run([self.summary, self.loss, self.auc], feed_dict=feed_dict)
        return summary, loss, auc
    
    def predict(self, user_input, pos_item_input, neg_item_input):
        feed_dict = {self.user_input: user_input,
                     self.pos_item_input: pos_item_input,
                     self.neg_item_input: neg_item_input}
        pred = self.sess.run([self.pred], feed_dict=feed_dict)
        return pred
    
    def similar(self, item_id, ascending=True):
        item_emb = self.item_emb[item_id, :]
        item_similarity = np.dot(item_emb, self.item_emb.T)
        sorted_idx = np.argsort(item_similarity)
        if ascending is False:
            sorted_idx = sorted_idx[::-1]
        results = np.hstack((sorted_idx.reshape(-1, 1), item_similarity[sorted_idx].reshape(-1, 1)))
        return results

    def recommend(self, user_id, ascending=True):
        user_emb = self.user_emb[user_id, :]
        user_recommend = np.dot(user_emb, self.item_emb.T)
        sorted_idx = np.argsort(user_recommend)
        if ascending is False:
            sorted_idx = sorted_idx[::-1]
        results = np.hstack((sorted_idx.reshape(-1, 1), user_recommend[sorted_idx].reshape(-1, 1)))
        return results
    
