import numpy as np
import os 
from sklearn.base import BaseEstimator, TransformerMixin
import tensorflow as tf
import time
try:
    get_ipython
    from tqdm import tqdm_notebook as tqdm
except:
    from tqdm import tqdm


class FMBPR(BaseEstimator, TransformerMixin):
    def __init__(self, feat_size, field_size, emb_size=8,
                 l2_reg=0.0,
                 optimizer="adam",
                 learning_rate=0.001,
                 epochs=10,
                 batch_size=256,
                 verbose=False,
                 random_seed=2012,
                 train_writer="./model",
                 test_writer=None,
                 log_device_placement=False):
        
        self.feat_size = feat_size
        self.field_size = field_size
        self.emb_size = emb_size

        self.l2_reg = l2_reg
        self.optimizer_type = optimizer
        self.learning_rate = learning_rate

        self.epochs = epochs
        self.batch_size = batch_size

        self.verbose = verbose
        self.random_seed = random_seed

        self.train_writer_path = train_writer
        self.test_writer_path = test_writer
        self.log_device_placement = log_device_placement

        self._init_graph()
    
    def _fm(self, feat_index, feat_value, name):
        nodes = dict()
        with tf.name_scope(name):
            # ---------- FM ----------
            # MODEL: calculation of FM in a straight forward approach
            # bias terms: shape = (None, )
            nodes["bias"] = tf.tile(self.weights["linear_bias"], [tf.shape(feat_index)[0]])

            # embedding values: v.x
            nodes["emb"] = tf.nn.embedding_lookup(self.weights["feat_emb"], feat_index)
            feat_value = tf.reshape(feat_value, shape=[-1, self.field_size, 1])
            nodes["emb"] = tf.multiply(nodes["emb"], feat_value)

            # linear term
            nodes["first_order"] = tf.nn.embedding_lookup(self.weights["feat_bias"], feat_index)
            nodes["first_order"] = tf.reduce_sum(tf.multiply(nodes["first_order"], feat_value), [1, 2])

            # second order embedding values: vx
            # shape = (None, emb_size): 0.5 * [(SUM(vx))^2 - SUM((vx)^2)]
            nodes["squared_sum"] = tf.square(tf.reduce_sum(nodes["emb"], 1))
            nodes["sum_squared"] = tf.reduce_sum(tf.square(nodes["emb"]), 1)
            nodes["second_order"] = tf.subtract(nodes["squared_sum"], nodes["sum_squared"])
            nodes["second_order"] = 0.5 * tf.reduce_sum(nodes["second_order"], 1)

            # output:
            nodes["output"] = tf.add_n([nodes["bias"], nodes["first_order"], nodes["second_order"]])
            nodes["output"] = tf.reshape(nodes["output"], shape=[-1, 1])

            return nodes
    
    def _init_graph(self):
        # reset the default graph to prevent the error between two graphs.
        tf.reset_default_graph()

        # create a new graph for the class and set it as default graph.
        self.graph = tf.Graph()
        with self.graph.as_default():
            # fixed the random_seed
            tf.set_random_seed(self.random_seed)

            # placeholders as the model inputs.
            self.pos_feat_index = tf.placeholder(tf.int32, 
                                                 shape=[None, self.field_size], 
                                                 name="pos_feature_index")
            self.pos_feat_value = tf.placeholder(tf.float32, 
                                                 shape=[None, self.field_size], 
                                                 name="pos_feature_value")
            self.neg_feat_index = tf.placeholder(tf.int32, 
                                                 shape=[None, self.field_size], 
                                                 name="neg_feature_index")
            self.neg_feat_value = tf.placeholder(tf.float32, 
                                                 shape=[None, self.field_size], 
                                                 name="neg_feature_value")
            
            # create variable weights to store the training variables of model
            self.weights = self._init_weights()
            
            # model
            self.model = dict()
            self.model["pos"] = self._fm(pos_feat_index, pos_feat_value, "pos")
            self.model["neg'] = self._fm(neg_feat_index, neg_feat_value, "neg")
            
            with tf.name_scope("Output"):
                self.subtract = self.model["pos"]["output"] - self.model["neg"]["output"]
                self.prob = tf.sigmoid(self.subtract)
                self.log_prob = tf.log(self.prob)
            
            with tf.name_scope("Loss"):
                self.loss = tf.reduce_sum(-self.log_prob)
            
            with tf.name_scope("Regularization"):
                if self.l2_reg > 0:
                    self.reg = 0
                    self.reg += tf.nn.l2_loss(self.weights["feat_emb"])
                    self.reg += tf.nn.l2_loss(self.weights["feat_emb"])
                    self.loss += self.l2_reg * self.reg
            
            with tf.name_scope("Train_Op"):
                if self.optimizer_type == "adam":
                    self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate, beta1=0.9, beta2=0.999, epsilon=1e-8).minimize(self.loss)
                elif self.optimizer_type == "adagrad":
                    self.optimizer = tf.train.AdagradOptimizer(learning_rate=self.learning_rate, initial_accumulator_value=1e-8).minimize(self.loss)
                elif self.optimizer_type == "gd":
                    self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
                elif self.optimizer_type == "momentum":
                    self.optimizer = tf.train.MomentumOptimizer(learning_rate=self.learning_rate, momentum=0.95).minimize(self.loss)
            
            self.saver = tf.train.Saver()
            self.sess = tf.Session(config=tf.ConfigProto(log_device_placement=self.log_device_placement))
            self.sess.run(tf.global_variables_initializer())
            
            # number of params
            if bool(self.verbose) is True:
                # number of params
                self.total_parameters = 0
                for v in self.weights.values():
                    shape = v.get_shape()
                    vparams = 1
                    for dim in shape:
                        vparams *= dim.value
                    self.total_parameters += vparams
                print(f"total_parameters of the model: {self.total_parameters}")
                
                '''
                to be updated - tf.summary
                '''
    
    def _init_weights(self):
        pass
    
    def fit_on_batch(self):
        pass
    
    def fit_on_batch_with_summary(self):
        pass
    
    def fit(self):
        pass
    
    def _evaluate(self):
        pass
    
    def predict(self):
        pass
    
    def save(self):
        pass
    
    def load(self):
        pass
