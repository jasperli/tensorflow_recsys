import numpy as np
import os
from sklearn.base import BaseEstimator, TransformerMixin
import tensorflow as tf
import time
try:
    get_ipython
    from tqdm import tqdm_notebook as tqdm
except:
    from tqdm import tqdm


class SimpleFactorizationMachine(BaseEstimator, TransformerMixin):
    def __init__(self, feat_size, field_size, emb_size=8,
                 l2_reg=0.0,
                 loss_type="logloss",
                 optimizer="adam",
                 learning_rate=0.001,
                 epochs=10,
                 batch_size=256,
                 verbose=False,
                 random_seed=2012,
                 train_writer="./model",
                 test_writer=None,
                 log_device_placement=False):
        '''
        Desc:
            To create a Factorization Machine Model with log-loss or mse loss function.

        Params:
        :feat_size:
        :field_size:
        :emb_size:
        :l2_reg:
        :loss_type:
        :optimizer:
        :learning_rate:
        :epochs:
        :batch_size:
        :verbose:
        :random_seed:
        :train_writer:
        :test_writer:
        :log_device_placement:

        Return:
        None

        '''
        assert loss_type in ["logloss", "mse"], \
            "loss type can be either 'logloss' for classification task or 'mse' for regression task"

        self.feat_size = feat_size
        self.field_size = field_size
        self.emb_size = emb_size

        self.l2_reg = l2_reg
        self.loss_type = loss_type
        self.optimizer_type = optimizer
        self.learning_rate = learning_rate

        self.epochs = epochs
        self.batch_size = batch_size

        self.verbose = verbose
        self.random_seed = random_seed

        self.train_writer_path = train_writer
        self.test_writer_path = test_writer
        self.log_device_placement = log_device_placement

        self._init_graph()


    def _fm(self, feat_index, feat_value, name):
        '''
        Desc:
            To compute a fm prediction by feat_index and feat_value

        Params:
        :feat_index:
        :feat_value:
        :name:

        Return:
        :self.nodes[name]["output"]
        '''
        nodes = dict()
        with tf.name_scope(name):
            # ---------- FM ----------
            # MODEL: calculation of FM in a straight forward approach
            # bias terms: shape = (None, )
            nodes["bias"] = tf.tile(self.weights["linear_bias"], [tf.shape(feat_index)[0]])

            # embedding values: v.x
            nodes["emb"] = tf.nn.embedding_lookup(self.weights["feat_emb"], feat_index)
            feat_value = tf.reshape(feat_value, shape=[-1, self.field_size, 1])
            nodes["emb"] = tf.multiply(nodes["emb"], feat_value)

            # linear term
            nodes["first_order"] = tf.nn.embedding_lookup(self.weights["feat_bias"], feat_index)
            nodes["first_order"] = tf.reduce_sum(tf.multiply(nodes["first_order"], feat_value), [1, 2])

            # second order embedding values: vx
            # shape = (None, emb_size): 0.5 * [(SUM(vx))^2 - SUM((vx)^2)]
            nodes["squared_sum"] = tf.square(tf.reduce_sum(nodes["emb"], 1))
            nodes["sum_squared"] = tf.reduce_sum(tf.square(nodes["emb"]), 1)
            nodes["second_order"] = tf.subtract(nodes["squared_sum"], nodes["sum_squared"])
            nodes["second_order"] = 0.5 * tf.reduce_sum(nodes["second_order"], 1)

            # output:
            nodes["output"] = tf.add_n([nodes["bias"], nodes["first_order"], nodes["second_order"]])
            nodes["output"] = tf.reshape(nodes["output"], shape=[-1, 1])

            return nodes


    def _init_graph(self):
        '''
        Desc:
            To initialize the graph of FM model with [log-loss|mse] loss.
            Given user_id u, item_id i, predict with FM model.

        Params:
        None

        Return:
        None
        '''
        # reset the default graph to prevent the error between two graphs.
        tf.reset_default_graph()

        # create a new graph for the class and set it as default graph.
        self.graph = tf.Graph()
        with self.graph.as_default():
            # fixed the random_seed
            tf.set_random_seed(self.random_seed)

            # placeholders as the model inputs.
            self.feat_index = tf.placeholder(tf.int32, shape=[None, self.field_size], name="feature_index")
            self.feat_value = tf.placeholder(tf.float32, shape=[None, self.field_size], name="feature_value")
            self.label = tf.placeholder(tf.float32, shape=[None, 1], name="label")

            # create variable weights to store the training variables of model
            self.weights = self._init_weights()

            # model
            self.model =self. _fm(self.feat_index, self.feat_value, "yhat")
            self.yhat = self.model["output"] # shape = (None, 1)

            # calculate the prediction
            with tf.name_scope("Loss"):
                if self.loss_type == "logloss":
                    self.prob = tf.nn.sigmoid(self.yhat)
                    self.loss = tf.losses.log_loss(self.label, self.prob)
                elif self.loss_type == "mse":
                    self.loss = tf.losses.mean_squared_error(self.label, self.yhat)

            with tf.name_scope("Regularization"):
                # l2 regularization on weights
                if self.l2_reg > 0:
                    self.reg = 0
                    self.reg += tf.nn.l2_loss(self.weights["feat_emb"])
                    self.reg += tf.nn.l2_loss(self.weights["feat_emb"])
                    self.loss += self.l2_reg * self.reg

            # train optimizer
            with tf.name_scope("Train_Op"):
                if self.optimizer_type == "adam":
                    self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate, beta1=0.9, beta2=0.999, epsilon=1e-8).minimize(self.loss)
                elif self.optimizer_type == "adagrad":
                    self.optimizer = tf.train.AdagradOptimizer(learning_rate=self.learning_rate, initial_accumulator_value=1e-8).minimize(self.loss)
                elif self.optimizer_type == "gd":
                    self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
                elif self.optimizer_type == "momentum":
                    self.optimizer = tf.train.MomentumOptimizer(learning_rate=self.learning_rate, momentum=0.95).minimize(self.loss)

            self.saver = tf.train.Saver()
            self.sess = tf.Session(config=tf.ConfigProto(log_device_placement=self.log_device_placement))
            self.sess.run(tf.global_variables_initializer())

            # number of params
            if bool(self.verbose) is True:
                # number of params
                self.total_parameters = 0
                for v in self.weights.values():
                    shape = v.get_shape()
                    vparams = 1
                    for dim in shape:
                        vparams *= dim.value
                    self.total_parameters += vparams
                print(f"total_parameters of the model: {self.total_parameters}")

                # tensorboard: tf.summary
                if self.train_writer_path is not None:
                    # summary of outputs
                    tf.summary.histogram("subtraction", self.yhat, collections=[tf.GraphKeys.SUMMARIES])
                    if self.loss_type == "logloss":
                        tf.summary.histogram("probability", self.prob, collections=[tf.GraphKeys.SUMMARIES])

                    # summary of loss
                    tf.summary.scalar("loss", self.loss, collections=[tf.GraphKeys.SUMMARIES])

                    # save the summary
                    self.summary = tf.summary.merge(tf.get_collection(tf.GraphKeys.SUMMARIES))
                    self.train_writer = tf.summary.FileWriter(self.train_writer_path, self.sess.graph)

                    if self.test_writer_path is not None:
                        # test_summary
                        self.test_summary = tf.summary.merge([tf.summary.scalar("test_loss", self.loss)])
                        self.test_writer = tf.summary.FileWriter(self.test_writer_path)


    def _init_weights(self):
        '''
        remark: add summary function to create variable
        '''
        weights = dict()
        with tf.name_scope("Weights"):
            # bias terms
            weights["linear_bias"] = tf.get_variable(
                "linear_bias",
                shape=[1],
                initializer=tf.random_normal_initializer(0.0, 1.0)
            )

            # embeddings
            # first order terms
            weights["feat_bias"] = tf.get_variable(
                "feature_bias",
                shape=[self.feat_size, 1],
                initializer=tf.random_normal_initializer(0.0, 1.0)
            )

            # second order terms
            weights["feat_emb"] = tf.get_variable(
                "feature_embedding",
                shape=[self.feat_size, self.emb_size],
                initializer=tf.random_normal_initializer(0.0, 0.01)
            )
        return weights


    def fit_on_batch(self, feat_index, feat_value, label):
        feed_dict = {self.feat_index: feat_index,
                     self.feat_value: feat_value,
                     self.label: label}
        loss, opt = self.sess.run([self.loss, self.optimizer], feed_dict=feed_dict)
        return loss


    def fit_on_batch_with_summary(self, feat_index, feat_value, label):
        feed_dict = {
            self.feat_index: feat_index,
            self.feat_value: feat_value,
            self.label: label,
        }
        summary, loss, opt = self.sess.run([self.summary, self.loss, self.optimizer], feed_dict=feed_dict)
        return summary, loss

    def fit(self, training_index, training_value, training_label,
            valid_index=None, valid_value=None, valid_label=None,
            early_stopping=False,
            save_step=0,
            save_path=None,
            summary_step=False):
        '''
        Desc:
            To fit the model with fit_on_batch per batch

        Params:
        :training_index:
        :training_value:
        :training_label:
        :valid_index:
        :valid_value:
        :valid_label:
        :early_stopping:
        :save_step:
        :save_path:
        :summary_step:

        Return:
        :history:
        '''
        # check if there is validation set
        has_valid = valid_index is not None

        # create a dictionary to store the historical metrics of the model.
        history = dict()
        history["loss"] = []
        if has_valid:
            history["val_loss"] = []

        # create variables for early_stopping by val_loss
        if has_valid and bool(early_stopping) is True:
            best_loss = np.inf
            unchange_time = 0

        # create folder in save_path
        if bool(self.verbose) is True and save_path is not None:
            if not os.path.exists(save_path+"/tmp_mdls"):
                os.makedirs(save_path+"/tmp_mdls")
            if not os.path.exists(save_path+"/best_mdls"):
                os.makedirs(save_path+"/best_mdls")
            if not os.path.exists(save_path+"/last_mdls"):
                os.makedirs(save_path+"/last_mdls")

        count_summary_step = 0
        samples_size = training_index.shape[0]

        for epoch in range(self.epochs):
            print(f"Epoch {epoch+1}/{self.epochs}")

            t1 = time.time()
            loss_ = []

            # create perm to shuffle the data (create a function to shuffle data in base.py)
            perm = np.random.permutation(samples_size)

            # create a processing bar of tqdm
            total_batch = samples_size // self.batch_size + 1
            pbar = tqdm(range(total_batch), desc="avg loss of batch: ?.????")

            for i in pbar:
                start_idx = self.batch_size * i
                end_idx = self.batch_size * (i + 1)

                if end_idx > samples_size:
                    sample_idx = perm[start_idx:]
                else:
                    sample_idx = perm[start_idx:end_idx]

                if bool(self.verbose) is True:
                    summary, loss = self.fit_on_batch_with_summary(training_index[sample_idx],
                                                                   training_value[sample_idx],
                                                                   training_label[sample_idx])
                else:
                    loss = self.fit_on_batch(training_index[sample_idx],
                                             training_value[sample_idx],
                                             training_label[sample_idx])

                # set desc of pbar
                pbar.set_description(f"avg loss of batch: {loss:.4f}" )
                pbar.refresh()

                # save the total loss of batch
                total_loss = loss * sample_idx.shape[0]
                loss_.append(total_loss)

                # save the summary per i-steps
                if bool(self.verbose) is True and summary_step is not False and (i+1) % int(summary_step) == 0:
                    self.train_writer.add_summary(summary, count_summary_step)
                    if has_valid and self.test_writer_path is not None:
                        val_summary, _ = self._evaluate(valid_index,
                                                        valid_value,
                                                        valid_label)
                        self.test_writer.add_summary(val_summary, count_summary_step)
                    count_summary_step += 1

            # save the loss of the epoch
            avg_loss = np.sum(loss_) / samples_size
            history["loss"].append(avg_loss)

            # print the info
            t2 = time.time() - t1

            # save the summary for each epoch
            if bool(self.verbose) is True and self.train_writer_path is not None:
                self.train_writer.add_summary(summary, count_summary_step)

            # save the model for each save_step
            if bool(self.verbose) is True and save_step > 0 and save_path is not None:
                if (epoch + 1) % save_step == 0:
                    self.saver.save(self.sess, save_path+"/tmp_mdls/model.ckpt", global_step=epoch)

            # compute and save the validation result
            if bool(self.verbose) is True and has_valid:
                val_summary, val_loss = self._evaluate(valid_index,
                                                       valid_value,
                                                       valid_label)
                history["val_loss"].append(val_loss)
                if self.test_writer_path is not None:
                    self.test_writer.add_summary(val_summary, count_summary_step)

                print(f"{t2:.2f}s - avg loss: {avg_loss:.4f} - val loss: {val_loss:.4f}")

                # early stop by validation loss
                if int(early_stopping) > 0:
                    if val_loss < best_loss:
                        best_loss = val_loss
                        unchange_time = 0
                        if save_path is not None:
                            self.saver.save(self.sess, save_path+"/best_mdls/model.ckpt")
                    else:
                        unchange_time += 1
                        if unchange_time >= int(early_stopping):
                            print(f"Early Stop is triggered for unchange_time={unchange_time}")
                            break
            else:
                print(f"{t2:.2f}s - avg loss: {avg_loss:.4f}")

            count_summary_step += 1

        # save the last model
        if bool(self.verbose) is True and save_path is not None:
            self.saver.save(self.sess, save_path+"/last_mdls/model.ckpt")

        return history


    def _evaluate(self, valid_index, valid_value, valid_label):
        feed_dict = {self.feat_index: valid_index,
                     self.feat_value: valid_value,
                     self.label: valid_label}
        val_summary, val_loss = self.sess.run([self.test_summary, self.loss], feed_dict=feed_dict)
        return val_summary, val_loss

    def predict(self, predict_index, predict_value):
        feed_dict = {self.feat_index: predict_index,
                     self.feat_value: predict_value}
        handle = self.sess.partial_run_setup([self.yhat], list(feed_dict.keys()))
        predict = self.sess.partial_run(handle, [self.yhat], feed_dict=feed_dict)
        return predict

    def save(self):
        pass

    def load(self):
        pass
